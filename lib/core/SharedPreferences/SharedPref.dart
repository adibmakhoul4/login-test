import 'dart:ffi';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  SharedPref._();
  SharedPreferences _preferences;
  static final SharedPref pref = SharedPref._();

  Future<SharedPreferences> get _getSharedPref async {
    if (_preferences != null)
      return _preferences;
    else {
      _preferences = await SharedPreferences.getInstance();
      return _preferences;
    }
  }

  setToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', value);
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  
  Future<Void> setfingerprintstate(bool state) async {
    final SharedPreferences p = await _getSharedPref;
    p.setBool("fingerstate", state);
  }

  Future<bool> getfingerprintstate() async {
    final SharedPreferences p = await _getSharedPref;
    bool id = p.getBool("fingerstate");
    return id;
  }


   setId(int id) async {
    final SharedPreferences p = await _getSharedPref;
    p.setInt("Id", id);
  }

  Future<int> getId() async {
    final SharedPreferences p = await _getSharedPref;
    int id = p.getInt("Id");
    return id;
  }
}
