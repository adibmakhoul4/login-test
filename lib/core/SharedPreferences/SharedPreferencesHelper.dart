

import 'package:test_app/core/constants/app-string.dart';

import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static bool initialized;
  static SharedPreferencesProvider _pref;
  static init() async {
    _pref = await SharedPreferencesProvider.getInstance();
  }

  //Lang
  static String get lang => _pref.read(AppStrings.LANG);
  static set lang(String lang) => _pref.save(AppStrings.LANG, lang);

  //first launch
  static bool get getFirstLunch {
    if (!_pref.contains(AppStrings.FIRSTLAUNCH)) {
      saveFirstLunch(true);
      return true;
    } else {
      return false;
    }
  }

  static saveFirstLunch(bool value) =>
      _pref.save(AppStrings.FIRSTLAUNCH, value);

  // save token
  static String get getToken => _pref.read(AppStrings.TOKEN);
  static saveToken(String token) => _pref.save(AppStrings.TOKEN, token);

  // save user id
  static String get getUserID => _pref.read(AppStrings.USERID);
  static saveUserID(String userID) => _pref.save(AppStrings.USERID, userID);
    // save user type
  static String get getUserType => _pref.read(AppStrings.USERTYPE);
  static saveUserType(String userType) => _pref.save(AppStrings.USERTYPE, userType);
    static String get getRestaurantID => _pref.read(AppStrings.RESTAURANTID);
   static String get getUserName => _pref.read(AppStrings.USERNAME);
  static saveUserName(String username) => _pref.save(AppStrings.USERNAME, username);

          static double get getlat => _pref.read(AppStrings.LAT);
  static saveLat(double lat) => _pref.save(AppStrings.LAT, lat);

          static double get getlong => _pref.read(AppStrings.LONG);
  static saveLong(double long) => _pref.save(AppStrings.LONG, long);
}
