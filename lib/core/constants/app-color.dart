import 'dart:ui';

class AppColors {
  static const Color PrimaryColor = Color(0xFFD81E41);
  static const Color Caian = Color(0xFF96D9C9);
  static const Color Green = Color(0xFF219653);
  static const Color Defulte = Color(0xFFFAFAFA);
  static const Color Light = Color(0xFFF4EFF1);
  static const Color Border = Color(0xFFD1D3D6);
  static const Color TextGreey = Color(0xFF9B9B9B);
  static const Color Greay = Color(0xFF828282);
  static const Color Text = Color(0xFF333333);
  static const Color Dark = Color(0xFF000000);
}
