class AppStrings {
  AppStrings._();
  static const LANG = "LANG";
  static const OTP = "OTP";
  static String TOKEN = "TOKEN";
  static String USERID = "USERID";
  static String USERTYPE = "USERTYPE";
  static String RESTAURANTID = "RESTAURANTID";
  static String SHOPID = "SHOPID";
  static String FCMTOKEN = "FCMTOKEN";
  static const FIRSTLAUNCH = "FIRSTLAUNCH";
  static const LAT = "LAT";
  static const LONG = "LONG";
  static String USERNAME = "USERNAME";

  static String CHANELNAME = "";
}
