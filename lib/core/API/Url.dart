class Url {
  static const BasicUserURL = "http://ank.motorseasons.sa/api/en/";
  static const RegisterURL = BasicUserURL + "auth/register";
  static const RegisterRestaurantURL = BasicUserURL + "api/services/app/Account/RegisterRestaurant";
  static const ChangePasswordURL = BasicUserURL + "auth/change-password";

  static const RegisterShopURL = BasicUserURL + "api/services/app/Account/RegisterShop";
  static const shopNotification = BasicUserURL + "notification/index";

  static const RegisterTrainerURL = BasicUserURL + "api/services/app/Account/RegisterTrainer";

  static const LoginURL = BasicUserURL + "auth/login";
  static const GetSliderURL = BasicUserURL + "lists/slider";
  static const GetLocationURL = BasicUserURL + "lists/location";

  static const GetProductURL = BasicUserURL + "product/index";
  static const GetCategoryURL = BasicUserURL + "product/categories";
  static const GetCobonsURL = BasicUserURL + "lists/coupons";
  static const SendSuggesstionURL = BasicUserURL + "suggestion/add";
  static const GetReviewsURL = BasicUserURL + "reviews/reviews";


  static const GetProductDetailsURL = BasicUserURL + "product/details";
  static const AddReviewProductDetailsURL = BasicUserURL + "reviews/add-edit-review";

  static const GetShopDetailsURL = BasicUserURL + "shop/details";

  static const GetShopURL = BasicUserURL + "shop/list";
  static const GetOrderURL = BasicUserURL + "order/list";
  static const GetOrderDetailsURL = BasicUserURL + "order/details";
  static const AddOrderURL = BasicUserURL + "order/new-order";

  static const CancleOrder = BasicUserURL + "order/cancel-order";
  static const OrderSummeryURL = BasicUserURL + "order/summery";
  static const CheckCopon = BasicUserURL + "lists/check-coupon";
  static const checkPoints = BasicUserURL + "lists/check-payment-method";


  static const FavoriteURL = BasicUserURL + "favorite/add-remove";





  static const checkCodeURL = BasicUserURL + "auth/check-code";
  static const editAddressURL = BasicUserURL + "auth/edit-address";
  static const addAddressURL = BasicUserURL + "auth/add-address";
  static const orderDetailsURL = BasicUserURL + "order/details";

  static const GetConversations = BasicUserURL + "chat/conversations";
  static const GetMessages = BasicUserURL + "chat/view";
  static const GetAgoraToken = BasicUserURL + "agora/get-rtm-token";
  static const SaveMessage = BasicUserURL + "chat/create";

  static const RequestOtpURL =
      BasicUserURL + "auth/reset-password";
  static const forgetPass =
      BasicUserURL + "auth/forget-password";
  static const SignupVerifyURL =
      BasicUserURL + "auth/verify-account";
  static const GetProfileURL = BasicUserURL + "auth/profile";
  static const UpdateProfileURL = BasicUserURL + "auth/update-profile";

  static const AboutURL = BasicUserURL + "app-info/about";
  static const PrivacyPolicyURL = BasicUserURL + "app-info/privacy-policy";
  static const TermsURL = BasicUserURL + "app-info/terms-and-conditions";

 

}