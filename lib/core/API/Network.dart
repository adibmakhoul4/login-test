import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:test_app/core/API/Url.dart';
import 'package:test_app/core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:test_app/user-management/data/ErrorModel.dart';
import 'package:test_app/user-management/data/LoginModel.dart';
import 'package:test_app/user-management/data/LoginResponceModel.dart';
import 'package:test_app/user-management/data/RegisterModel.dart';
import 'package:test_app/user-management/data/RegisterResponceModel.dart';
import 'package:test_app/user-management/data/messageModel.dart';

class Network {
  static var dio;
  static Response response;
  static initial() {
    BaseOptions options = new BaseOptions(headers: {
      "Accept-Language": 'en',
      'Authorization': AppSharedPreferences.getToken!=null?"Bearer ${AppSharedPreferences.getToken.substring(1,AppSharedPreferences.getToken.length-1)}":AppSharedPreferences.getToken,
    });
    dio = Dio(options);
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
  }
///////// user management
  static Future<RegisterResponceModel> register(
      RegisterModel registerModel) async {
    RegisterResponceModel registerResponceModel;
    var dataJson=registerModel.toJson();
    try {
      response = await dio.post(Url.RegisterURL, data: dataJson);
 
      registerResponceModel = RegisterResponceModel.fromJson(
          response?.data ?? RegisterResponceModel(isOk: false));
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout) {
        registerResponceModel = RegisterResponceModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect Timeout"));
      } else if (error.type == DioErrorType.sendTimeout) {
        registerResponceModel = RegisterResponceModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Send Timeout"));
      } else if (error.type == DioErrorType.other) {
        registerResponceModel = RegisterResponceModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Server error"));
      } else if (error.type == DioErrorType.cancel) {
        registerResponceModel = RegisterResponceModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect cance"));
      } else if (error.type == DioErrorType.receiveTimeout) {
        registerResponceModel = RegisterResponceModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Receive Timeout"));
      } else {
        registerResponceModel =
            RegisterResponceModel.fromJson(error.response?.data);
      }
    } catch (e) {
      registerResponceModel = RegisterResponceModel(
          error: ErrorModel(message: e.toString(), code: null),
         
          isOk: false);
    }
    return registerResponceModel;
  }

 static Future<LoginResponceModel> login(LoginModel loginModel) async {
    LoginResponceModel loginResponceModel;
      String resultText;
    try {
      response = await dio.post(Url.LoginURL, data: loginModel.toJson());
 loginResponceModel = LoginResponceModel.fromJson(
          response?.data ?? LoginResponceModel(isOk: false));
        String token = response.data['result']['token'];
        int resultCode = response.data['result']['resultCode'];
         resultText = response.data['result']['resultText'];

        if(response.data['result']['profile']!=null)
    loginResponceModel.token = token;
    loginResponceModel.resultCode = resultCode;
    loginResponceModel.resultText = resultText;



     

    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout) {
        loginResponceModel = LoginResponceModel(
            isOk: false,
            messageModel:  MessageModel(code: 120, content: "Connect Timeout"));
      } else if (error.type == DioErrorType.sendTimeout) {
        loginResponceModel = LoginResponceModel(
            isOk: false,
            messageModel:  MessageModel(code: 120, content:"Send Timeout"));
      } else if (error.type == DioErrorType.other) {
        loginResponceModel = LoginResponceModel(
                isOk: false,
            messageModel:  MessageModel(code: 120, content: "Server error"));
      } else if (error.type == DioErrorType.cancel) {
        loginResponceModel = LoginResponceModel(
          isOk: false,
            messageModel:  MessageModel(code: 120, content: "Connect cance"));
      } else if (error.type == DioErrorType.receiveTimeout) {
        loginResponceModel = LoginResponceModel(
           isOk: false,
            messageModel:  MessageModel(code: 120, content: "Receive Timeout"));
      } else {
        loginResponceModel = LoginResponceModel.fromJson(
            error.response?.data ?? LoginResponceModel(isOk: false));
      }
    } catch (e) {
      loginResponceModel = LoginResponceModel(
                  isOk: false,
            messageModel:  MessageModel(code: 120, content: resultText.toString()));
         
    }
    return loginResponceModel;
  }

}
 