import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-color.dart';

class CatogoriesWidget extends StatefulWidget {
  final String image;
  final String title;
  final Function onTap;

  CatogoriesWidget({
     this.image,
     this.title,
     this.onTap,
  });

  @override
  _CatogoriesWidgetState createState() => _CatogoriesWidgetState();
}

class _CatogoriesWidgetState extends State<CatogoriesWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 2.w),
      child: GestureDetector(
      onTap: () {
        widget.onTap();
      },
      child:  Container(
        width: 16.h,
        height: 18.h,
        decoration: BoxDecoration(
            color: AppColors.Light,
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
                    widget.image,
                    height: 10.h,
                    width: 30.w,
                    fit: BoxFit.cover,
                  ),
           
           Expanded(
             child: Text(
              widget.title,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w400,
                color: AppColors.Text,
              ),
            ),
           ) 
          ],
        ),
      ),
    ));
  }
}
