import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class Sex extends StatefulWidget {
  const Sex({Key key}) : super(key: key);

  @override
  _SexState createState() => _SexState();
}

class _SexState extends State<Sex> {
  String dropdownvalue = 'ذكر';
  var items = [
    'ذكر',
    'انثى',
  ];
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 8.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.h),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    value: dropdownvalue,
                    icon: Icon(
                      Icons.arrow_drop_down,
                    ),
                    items: items.map((String items) {
                      return DropdownMenuItem(value: items, child: Text(items));
                    }).toList(),
                    onChanged: (String newValue) {
                      setState(
                        () {
                          dropdownvalue = newValue;
                        },
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                'الجنس',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
