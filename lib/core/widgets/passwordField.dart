import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-color.dart';

class PasswordField extends StatefulWidget {
  final String title;
  PasswordField({
      this.title,
  });

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 8.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextField(
                    obscureText: true,
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                      suffixIcon: IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.visibility_outlined,
                          color: AppColors.Greay,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                widget.title,
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
