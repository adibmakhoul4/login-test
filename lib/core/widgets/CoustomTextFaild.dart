import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-color.dart';

class CoustomTextFaild extends StatefulWidget {
  final String title;
  CoustomTextFaild({
      this.title,
  });
  @override
  _CoustomTextFaildState createState() => _CoustomTextFaildState();
}

class _CoustomTextFaildState extends State<CoustomTextFaild> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 8.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextField(
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
          child: Container(
            color: AppColors.Defulte,
            child: Center(
              child: Text(
                widget.title,
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
