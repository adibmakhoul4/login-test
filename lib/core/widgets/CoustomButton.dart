import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CoustomButton extends StatefulWidget {
  final String title;
  final Function onClick;
  final Color buttonColor;
  final Color fontColor;
  CoustomButton(
      { this.title,
       this.onClick,
       this.buttonColor,
       this.fontColor});
  @override
  _CoustomButtonState createState() => _CoustomButtonState();
}

class _CoustomButtonState extends State<CoustomButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        width: double.infinity,
        height: 6.5.h,
        child: ElevatedButton(
          child: Text("${widget.title}".toUpperCase(),
              style: TextStyle(
                  color: widget.fontColor, fontWeight: FontWeight.bold)),
          style: ButtonStyle(
              foregroundColor:
                  MaterialStateProperty.all<Color>(widget.buttonColor),
              backgroundColor:
                  MaterialStateProperty.all<Color>(widget.buttonColor),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      side: BorderSide(color: widget.buttonColor)))),
          onPressed: () {
            widget.onClick();
          },
        ),
      ),
    );
  }
}
