import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-color.dart';

class PhoneNumberField extends StatefulWidget {
  const PhoneNumberField({ Key key}) : super(key: key);

  @override
  _PhoneNumberFieldState createState() => _PhoneNumberFieldState();
}

class _PhoneNumberFieldState extends State<PhoneNumberField> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Stack(
        children: [
          SizedBox(
            height: 8.h,
            child: Padding(
              padding: EdgeInsets.only(top: 2.h),
              child: Container(
                width: double.infinity,
                height: 6.h,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: AppColors.Border,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 28.w,
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(color: AppColors.Border, width: 1),
                        ),
                      ),
                      child: CountryCodePicker(
                        padding: EdgeInsets.zero,
                        onChanged: print,
                        initialSelection: 'SA',
                        favorite: ['+966', 'KSA'],
                      ),
                    ),
                    Container(
                      width: 60.w,
                      height: 6.h,
                      child: Directionality(
                        textDirection: TextDirection.ltr,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: TextFormField(
                            maxLength: 10,
                            cursorColor: Colors.black,
                            cursorWidth: 0.5,
                            decoration: InputDecoration(

                                // suffixIcon: Image.asset(
                                //   AppAssets.Phone,
                                //   height: 1.h,
                                // ),
                                counter: Offstage(),
                                border: InputBorder.none,
                                errorBorder: InputBorder.none),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
              right: 5.w,
              child: Container(
                width: 20.w,
                color: AppColors.Defulte,
                child: Center(
                  child: Text(
                    'رقم الجوال',
                    style: TextStyle(fontSize: 11.sp),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
