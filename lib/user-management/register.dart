import 'dart:io';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_toastr/flutter_toastr.dart';
import 'package:sizer/sizer.dart';
import 'package:path/path.dart';
import 'package:test_app/core/constants/app-assets.dart';
import 'package:test_app/core/constants/app-color.dart';
import 'package:test_app/core/widgets/CoustomButton.dart';
import 'package:test_app/user-management/data/RegisterModel.dart';

import 'domin/cubit/user_mangment_cubit.dart';
import 'home.dart';
import 'login.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
      TextEditingController name = TextEditingController();
      TextEditingController id_card_number = TextEditingController();
      TextEditingController enname = TextEditingController();
      TextEditingController arname = TextEditingController();
  TextEditingController email = TextEditingController();
  CountryCode countryCode = CountryCode(name: "KSA", code: "+966");
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController phone = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController verifyPassword = TextEditingController();
 String dropdownvalue;
  var items = [
    'male',
    'female',
  ];
  
  UserMangmentCubit userMangmentCubit = new UserMangmentCubit();

  bool isChecked = false;
  File file;
  File file1;
  File file2;
  File file3;

  bool _obscureText = true;

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  bool _obscureText1 = true;


  // Toggles the password show status
  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  @override
  Widget build(BuildContext context) {
      Future<bool> onPopScope2() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: TextDirection.ltr,
          child: AlertDialog(
            content: Text(
            'Are you sure?',
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                 'no',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'yes',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
            ],
          ),
        );
      },
    ).then((value) => value ?? false);
  }

   return Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: Colors.white,
            title: Text(
             'Sign up',
              style: TextStyle(
                color: AppColors.PrimaryColor,
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
         
             body: Form(
                        key: formKey,
                        child: Container(
                          child: BlocProvider(
                            create: (context) => userMangmentCubit,
                            child: BlocConsumer<UserMangmentCubit,
                                UserMangmentState>(
                                    listener: (context, state) {
                        if (state is RegisterFailur) {
                                     Navigator.pop(context, true);

                          FlutterToastr.show(
                              state.errorModel.message ?? state.errorModel.message,
                              context,
                              duration: FlutterToastr.lengthShort,
                              position: FlutterToastr.bottom);
                        } else if (state is RegisterSucess) {
                                     Navigator.pop(context, true);

                          FlutterToastr.show('Successfully', context,
                              duration: FlutterToastr.lengthShort,
                              position: FlutterToastr.bottom);
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MyHomePage()
                          ));
                        }else{
                          if(state is  RegisterLoading){
                            setState(() {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Center(
              child: CircularProgressIndicator(color: Colors.red,),
            );
          });
    });
                          }
                        }
                        
                      }, builder: (context, state) {
                              return Container(
                                  width: 1000.0.w,
                                  child: Stack(
                                    children: [
                                      SingleChildScrollView(
                                        child:
                                           Padding(
              padding: EdgeInsets.symmetric(horizontal: 2.h),
           child:
                                        
                                         Column(
                children: [
				        Center(
                      child: Image.asset(
                        AppAssets.ANKRed,
                        width: 30.w,
                        height: 20.h,
                      ),
                    ),
                  Stack(
      children: [
        SizedBox(
          height: 17.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextFormField(
                    controller: name,
    validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                 
                                    return null;
                                  },
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
           top: -1,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                'Full name',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
 
                    SizedBox(
                      height: 3.h,
                    ),
                  Stack(
        children: [
		      
          SizedBox(
            height: 17.h,
            child:Directionality(
                        textDirection: TextDirection.ltr,
                        child: Padding(
              padding: EdgeInsets.only(top: 2.h),
              child: Container(
                width: double.infinity,
                height: 6.h,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: AppColors.Border,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 28.w,
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(color: AppColors.Border, width: 1),
                        ),
                      ),
                      child: CountryCodePicker(
                        padding: EdgeInsets.zero,
                          onChanged: (value) {
                                                        countryCode = value;
                                                        setState(() {});
                                                      },
                        initialSelection: 'SA',
                        favorite: ['+966', 'KSA'],
                      ),
                    ),
                    Container(
                      width: 60.w,
                      height: 6.h,
                      child: Directionality(
                        textDirection: TextDirection.ltr,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: TextFormField(
                    controller: phone,
                          validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    if(val.length<9){
                                      return '9 characters at least';
                                    }
                                    return null;
                                  },
                            maxLength: 10,
                            cursorColor: Colors.black,
                            cursorWidth: 0.5,
                            decoration: InputDecoration(

                                // suffixIcon: Image.asset(
                                //   AppAssets.Phone,
                                //   height: 1.h,
                                // ),
                                counter: Offstage(),
                                border: InputBorder.none,
                                errorBorder: InputBorder.none),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),)
          ),
          Positioned(
              right: 5.w,
               top: -1,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: Text(
                    'Phone number',
                    style: TextStyle(fontSize: 11.sp),
                  ),
                ),
              )),
        ],
      ),
   
                    SizedBox(
                      height: 3.h,
                    ),
                   Stack(
      children: [
        SizedBox(
          height: 17.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextFormField(
                    controller: email,
    validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    if(val.isEmpty || !val.contains("@")){
                                      return 'format error';
                                    }
                                    return null;
                                  },
                             
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
           top: -1,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                'Email',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
 
                    SizedBox(
                      height: 3.h,
                    ),
                       Stack(
                                          children: [
                                            SizedBox(
                                              height: 10.h,
                                              child: Padding(
                                                padding:
                                                    EdgeInsets.only(top: 2.h),
                                                child: Container(
                                                  width: double.infinity,
                                                  height: 6.h,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: Colors.black,
                                                      width: 0.4,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(5),
                                                    ),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 2.h),
                                                    child:
                                                        DropdownButtonHideUnderline(
                                                      child: DropdownButton(
                                                        value: dropdownvalue,
                                                        icon: Icon(
                                                          Icons.arrow_drop_down,
                                                        ),
                                                        items: items
                                                            .map((String
                                                                items) {
                                                          return DropdownMenuItem(
                                                              value: items,
                                                              child: Text(
                                                                  items));
                                                        }).toList(),
                                                        onChanged:
                                                            (String
                                                                newValue) {
                                                          setState(
                                                            () {
                                                              dropdownvalue =
                                                                  newValue;
                                                             
                                                            },
                                                          );
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                                right: 5.w,
           top: -1,

                                              child: Container(
                                                color: Colors.white,
                                                child: Center(
                                                  child: Text(
                                                    'Gender',
                                                    style: TextStyle(
                                                        fontSize: 11.sp),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                             SizedBox(
                      height:3.h,
                    ),         
                  Stack(
      children: [
        SizedBox(
          height: 17.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextFormField(
                    controller: password,
 validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    if(val.length<8){
                                      return '8 characters at least!';
                                    }
                                    return null;
                                  },
                    obscureText: _obscureText,
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                      suffixIcon: IconButton(
                        onPressed: () {_toggle();},
                      icon: Icon(
                           _obscureText
               ? Icons.visibility
               : Icons.visibility_off,
                          color: AppColors.Greay,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
           top: -1,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                'Password',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
 
              
                    SizedBox(
                      height:3.h,
                    ),
                   Stack(
      children: [
        SizedBox(
          height: 17.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 6.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 1.h, right: 3.w),
                  child: TextFormField(
                    controller: verifyPassword,
 validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    if(val.length<8){
                                      return '8 characters at least!';
                                    }
                                    if(val != password.text){
                                      return 'not match!';
                                    }
                                    return null;
                                  },
                    obscureText: _obscureText1,
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                      suffixIcon: IconButton(
                        onPressed: () {_toggle1();},
                        icon: Icon(
                           _obscureText1
               ? Icons.visibility
               : Icons.visibility_off,
                          color: AppColors.Greay,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
           top: -1,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
               'Re-Password',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
 

                   SizedBox(
                      height: 3.h,
                    ),
 Wrap(
   children: [
Row(
   children: [      Container(
                          width: 5.w,
                          child: Checkbox(
                            checkColor: Colors.white,
                            fillColor: MaterialStateProperty.resolveWith(
                              (states) => AppColors.PrimaryColor,
                            ),
                            value: isChecked,
                            onChanged: (value) {
                              setState(() {
                                if (isChecked == false) {
                                  isChecked = true;
                                } else {
                                  isChecked = false;
                                }
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Text(
                        'Agree with',
                          style: TextStyle(
                            color: AppColors.Text,
                            fontSize: 10.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                       Text(
                          ' all the ',
                            style: TextStyle(
                              color: AppColors.PrimaryColor,
                              fontSize: 10.sp,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        
                        Text(
                          'permissions',
                          style: TextStyle(
                            color: AppColors.Text,
                            fontSize: 10.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                     
                   ],
 ),
       
   ],
 )
 ,            SizedBox(
                      height: 5.h,
                    ),
                    CoustomButton(
                      title: 'Sign up',
                         onClick: () {
                      if (formKey.currentState.validate())
{
  if(isChecked ){
     userMangmentCubit.register(RegisterModel(
                              phone:countryCode.dialCode!=null?countryCode.dialCode+ phone.text:'+966'+phone.text,
                               password: password.text,
                               email: email.text.trim(),
                               first_name:  name.text,
                               last_name: name.text,
                               gender:  dropdownvalue=='male' ? 1 : 2
                            ));
                        }else{
                            FlutterToastr.show(
                             'You should accept all the permissions and restrictions.',
                              context,
                              duration: FlutterToastr.lengthShort,
                              position: FlutterToastr.bottom);
                        }

}
                      
                    
                      },
                  
                      buttonColor: AppColors.Dark,
                      fontColor: Colors.white,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Do you have an account?",
                          style: TextStyle(
                            color: AppColors.Text,
                            fontSize: 10.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignIn()));
                          },
                          child: Text(
                            "Sign in",
                            style: TextStyle(
                              color: AppColors.PrimaryColor,
                              fontSize: 10.sp,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
            
                ]  ),  ),
                                   ),   
                      
                                    ],
                                  ));
                            }),
                          ),
                        ),
                      ),
                   
          
          
          
          
          
          
          
          
          
          
          
          
         ));
           
        }
 

}
