import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:test_app/core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:test_app/core/constants/app-color.dart';
import 'package:test_app/user-management/login.dart';
import 'package:test_app/user-management/welcome.dart';

class MyHomePage extends StatefulWidget {

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    String user;
    if(FirebaseAuth.instance.currentUser!=null){
   user = FirebaseAuth.instance.currentUser.email;

    }
  if(user == null){
    user= 'User';
  }
 return Scaffold(
      appBar: AppBar(title: Text("welcome"),
      actions: [IconButton(onPressed: (){
  auth.signOut();

        Navigator.pushAndRemoveUntil(context, 
        MaterialPageRoute(builder: (context)=> SignIn()), (route) => false);
      },
       icon: Icon(Icons.exit_to_app))],),
      body: Center(child: Text("Welcome $user"),),
    );
      
}}
