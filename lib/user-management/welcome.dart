
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-assets.dart';
import 'package:test_app/core/constants/app-color.dart';
import 'package:test_app/user-management/register.dart';
import 'package:test_app/user-management/widgets/button.dart';

import 'login.dart';

class Sign extends StatefulWidget {
  const Sign({Key key}) : super(key: key);

  @override
  _SignState createState() => _SignState();
}

class _SignState extends State<Sign> {
  Future<bool> onPopScope2() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: TextDirection.ltr,
          child: AlertDialog(
            content: Text(
             'Are you sure?',
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'no',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                 'yes',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
            ],
          ),
        );
      },
    ).then((value) => value ?? false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onPopScope2,
      child: Scaffold(
        body: Stack(
          children: [
            Container(

              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AppAssets.Splash4),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AppAssets.SplashShadow),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: SafeArea(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2.h),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.h,
                      ),
                      Image.asset(
                        AppAssets.ANKRed,
                        width: 70.w,
                      ),
                      Text(
                       'Welcome user',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Spacer(
                        flex: 10,
                      ),
                      SignButton(
                          title: 'Sign in',
                          onClick: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignIn()),
                                (route) => false);
                          }),
                      SizedBox(
                        height: 2.h,
                      ),
                      SignButton(
                        title: 'Create new account',
                        onClick: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => SignUp()),
                              (route) => false);
                        },
                      ),
                     Spacer(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
