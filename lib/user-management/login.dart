
import 'package:country_code_picker/country_code.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_toastr/flutter_toastr.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/core/constants/app-assets.dart';
import 'package:test_app/core/constants/app-color.dart';
import 'package:test_app/core/widgets/CoustomButton.dart';
import 'package:test_app/user-management/register.dart';

import 'data/LoginModel.dart';
import 'domin/cubit/user_mangment_cubit.dart';
import 'home.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  Future<bool> onPopScope2() {
     return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: TextDirection.ltr,
          child: AlertDialog(
            content: Text(
            'Are you sure?',
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                 'no',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text(
                  'yes',
                  style: TextStyle(color: AppColors.Text),
                ),
              ),
            ],
          ),
        );
      },
    ).then((value) => value ?? false);
 }
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

    UserMangmentCubit userMangmentCubit = new UserMangmentCubit();
  CountryCode countryCode = CountryCode(name: "KSA", code: "+966");
  TextEditingController phone = TextEditingController();
  TextEditingController password = TextEditingController();
   bool _obscureText = true;


  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
  return WillPopScope(
      onWillPop: onPopScope2,
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: Colors.white,
            title: Text(
              'Sign in',
              style: TextStyle(
                color: AppColors.PrimaryColor,
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
         
             body: Form(
                        key: formKey,
                        child: Container(
                          child: BlocProvider(
                            create: (context) => userMangmentCubit,
                            child: BlocConsumer<UserMangmentCubit,
                                UserMangmentState>(
                                  listener: (context, state) {
                                   if (state is LoginFailur) {
                                     Navigator.pop(context, true);
                          FlutterToastr.show(
                              state.errorModel.message ?? "Error in sign in",
                              context,
                              duration: FlutterToastr.lengthShort,
                              position: FlutterToastr.bottom);
                        } 
                        
                         if (state is LoginSucess) {
                                     Navigator.pop(context, true);

                          FlutterToastr.show("Sign in successfully", context,
                              duration: FlutterToastr.lengthShort,
                              position: FlutterToastr.bottom);
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>MyHomePage()
                          ));
                        }
                          if(state is  LoginLoading){
                            setState(() {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Center(
              child: CircularProgressIndicator(color: Colors.red,),
            );
          });
    });
                          
                        }
                            }, builder: (context, state) {
                              return Container(
                                  width: 1000.0.w,
                                  child: Stack(
                                    children: [
                                      SingleChildScrollView(
                                        child:                           Padding(
              padding: EdgeInsets.symmetric(horizontal: 2.h),
           child:
                                        Column(
                children: [
                  SizedBox(
                    height: 7.h,
                  ),
                  Image.asset(
                    AppAssets.ANKRed,
                    width: 40.w,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Directionality(
      textDirection: TextDirection.ltr,
      child:    Stack(
      children: [
         SizedBox(
            height: 17.h,
            child: Padding(
              padding: EdgeInsets.only(top: 2.h),
              child: Container(
              width: double.infinity,
              height: 2.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
                child: Row(
                  children: [
                    Container(
                      width: 28.w,
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(color: AppColors.Border, width: 1),
                        ),
                      ),
                      child: CountryCodePicker(
                        padding: EdgeInsets.zero,
                            onChanged: (value) {
                                                        countryCode = value;
                                                        setState(() {});
                                                      },
                        initialSelection: 'SA',
                        favorite: ['+966', 'KSA'],
                      ),
                    ),
                    Container(
                      width: 60.w,
                      height: 6.h,
                      child: Directionality(
                        textDirection: TextDirection.ltr,
                        child: Padding(
                          padding: EdgeInsets.only( right: 3.w),
                          child: TextFormField(
                              validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    return null;
                                  },
                            controller: phone,
                            maxLength: 10,
                            cursorColor: Colors.black,
                            cursorWidth: 0.5,
                            decoration: InputDecoration(

                                // suffixIcon: Image.asset(
                                //   AppAssets.Phone,
                                //   height: 1.h,
                                // ),
                                counter: Offstage(),
                                border: InputBorder.none,),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
          right: 5.w,
           top: -1,
          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
                'Phone number',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
  ),
        
                  SizedBox(
                    height: 2.h,
                  ),
                 Stack(
      children: [
        SizedBox(
          height:17.h,
          child: Padding(
            padding: EdgeInsets.only(top: 2.h),
            child: Container(
              width: double.infinity,
              height: 2.h,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 0.4,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Container(
                height: 6.h,
                child: Padding(
                  padding: EdgeInsets.only(top: 1.h, right: 3.w),
                  child: TextFormField(
                       validator: (val) {
                                    if (val.isEmpty) {
                                      return 'required field';
                                    }
                                    if(val.length<8){
                                      return '8 characters at least!';
                                    }
                                    return null;
                                  },
                    controller: password,
                    obscureText: _obscureText,
                    cursorColor: Colors.black,
                    cursorWidth: 0.5,

                    decoration: InputDecoration(
                      border: InputBorder.none,
                      errorBorder: InputBorder.none,
                      suffixIcon: IconButton(
                        onPressed: () {_toggle();},
                     icon: Icon(
                           _obscureText
               ? Icons.visibility
               : Icons.visibility_off,
                          color: AppColors.Greay,
                        ),
                      ),
                        contentPadding:
      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                 
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 5.w,
           top: -1,

          child: Container(
            color: Colors.white,
            child: Center(
              child: Text(
               'Password',
                style: TextStyle(fontSize: 11.sp),
              ),
            ),
          ),
        ),
      ],
    ),
   SizedBox(
                    height: 1.h,
                  ),
                     GestureDetector(
                   onTap: (){
               userMangmentCubit.signWithGoogle().then((UserCredential value){
                       final displayName= value.user.displayName;
                       print(displayName);
                       Navigator.pushAndRemoveUntil(context, 
                       MaterialPageRoute(builder: (context)=>MyHomePage()), (route) => false);
                     });;
                   },
                   child: Ink(
    color: Colors.red[600],
    child: Padding(
      padding: EdgeInsets.all(6),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Icon(Icons.email), // <-- Use 'Image.asset(...)' here
          SizedBox(width: 12),
          Text('Sign in with Google'),
        ],
      ),
    ),
  ),
                 ),
                
                  SizedBox(
                    height: 10.h,
                  ),
                  CoustomButton(
                    title: 'Sign in',
                    onClick: () {
                      if (formKey.currentState.validate())
                                    {
                              userMangmentCubit.login(
                                                    context: context,
                                                      loginModel: LoginModel(
                                                          password:password.text,
                                                          phone:countryCode.dialCode!=null? countryCode.dialCode+ phone.text:'+966'+phone.text));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => MyHomePage()));
                     } },
                    buttonColor: AppColors.Dark,
                    fontColor: Colors.white,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Divider(
                    thickness: 2,
                  ),
                
                  Wrap(
                    children: [
                      Text(
                        "You don't have an account?",
                        style: TextStyle(
                          color: AppColors.Text,
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      GestureDetector (
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) => SignUp()));
                        },
                        child: Text(
                          "Create new account.",
                          style: TextStyle(
                            color: AppColors.PrimaryColor,
                            fontSize: 10.sp,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3.h,
                  )
                ],
              ),)  ),
                                      // Visibility(
                                      //     visible: (state is LoginLoading),
                                      //     child: Container(
                                      //       child: Center(
                                      //           child: CircularProgressIndicator(
                                      //               valueColor:
                                      //                   AlwaysStoppedAnimation<
                                      //                       Color>(Theme.of(
                                      //                           context)
                                      //                       .primaryColor))),
                                      //     ))
                                    ],
                                  ));
                            }),
                          ),
                        ),
                      ),
                   
          
          
          
          
          
          
          
          
          
          
          
          
         )));
            
       


  }
}
