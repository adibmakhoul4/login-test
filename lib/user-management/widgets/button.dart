import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class SignButton extends StatefulWidget {
  final String title;
  final Function onClick;
  SignButton({
     this.title,
     this.onClick,
  });
  @override
  _SignButtonState createState() => _SignButtonState();
}

class _SignButtonState extends State<SignButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 6.h,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Colors.white,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        onPressed: () {
          widget.onClick();
        },
        child: Text(
          widget.title,
          style: TextStyle(
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
