class MessageModel {
  String type;
  int code;
  String content;

  MessageModel({ this.type,  this.code, this.content});

  MessageModel.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    code = json['code'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['code'] = this.code;
    data['content'] = this.content;
    return data;
  }
}