
import 'ErrorModel.dart';

class RegisterRestResponceModel {
  Result result;
  String targetUrl;
  bool success;
  ErrorModel error;
  bool unAuthorizedRequest;
  bool bAbp;

  RegisterRestResponceModel(
      {this.result,
      this.targetUrl,
      this.success,
      this.error,
      this.unAuthorizedRequest,
      this.bAbp});

  RegisterRestResponceModel.fromJson(Map<String, dynamic> json) {
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
    targetUrl = json['targetUrl'];
    success = json['success'];
      error =
        json['error'] != null ? new ErrorModel.fromJson(json['error']) : null;
    unAuthorizedRequest = json['unAuthorizedRequest'];
    bAbp = json['__abp'];
  }
}

class Result {
  bool canLogin;

  Result({this.canLogin});

  Result.fromJson(Map<String, dynamic> json) {
    canLogin = json['canLogin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['canLogin'] = this.canLogin;
    return data;
  }
}
