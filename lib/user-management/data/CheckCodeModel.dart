
class CheckCodeModel {
  String phone;
  String code;
  int codeType;
  CheckCodeModel({ this.phone,  this.code,this.codeType});

  CheckCodeModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    code = json['code'];
    codeType = json['codeType'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['code'] = this.code;
    data['codeType'] = this.codeType;

    return data;
  }
}