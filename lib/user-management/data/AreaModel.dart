
import 'CityModel.dart';

class AreaModel {
  int id;
  String name;
  String phone;
  String address;
  CityModel cityModel;
  AreaModel({ this.id,  this.name,this.phone,this.address});

  AreaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    address = json['address'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['address'] = this.address;

    return data;
  }
}