
import 'dart:io';

import 'AddressModel.dart';
import 'AreaModel.dart';

class EditProfileModel {
  int id;
  String first_name;
  String last_name;
  String phone;
  String email;
  File image;
  int total_points;
  int gender;
  String birthDate;
  int wallet;
  List<AreaModel> addresses;
  List<AddressModel> addressList;

  EditProfileModel({ this.id,  this.first_name,this.last_name,this.phone,this.email,this.image,this.total_points,
  this.gender ,this.birthDate, this.wallet});

  EditProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    first_name = json['first_name'];
    last_name = json['last_name'];
    phone = json['phone'];
        email = json['email'];
        total_points = json['total_points'];
        birthDate = json['birthDate'];
        gender = json['gender'];

    wallet = json['wallet'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.first_name;
    data['last_name'] = this.last_name;
    data['phone'] = this.phone;
    data['email'] = this.email;
       data['total_points'] = this.total_points;
       data['birthDate'] = this.birthDate;
       data['gender'] = this.gender;


    data['wallet'] = this.wallet;
    return data;
  }
}