

import 'ErrorModel.dart';

class ChangePasswordResponceModel {
    bool isOk;
  ErrorModel error;
  

  ChangePasswordResponceModel(
      {
      this.isOk,
      this.error,
     });

  ChangePasswordResponceModel.fromJson(Map<String, dynamic> json) {
    isOk = json['isOk'];
    error = json['error'] != null ? new ErrorModel.fromJson(json['error']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isOk'] = this.isOk;
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}
