import 'ErrorModel.dart';

class ForgetPasswordResponceModel {
  dynamic result;
  dynamic targetUrl;
  bool isOk;
  ErrorModel error;
  bool unAuthorizedRequest;
  bool bAbp;

  ForgetPasswordResponceModel(
      {this.result,
      this.targetUrl,
      this.isOk,
      this.error,
      this.unAuthorizedRequest,
      this.bAbp});

  ForgetPasswordResponceModel.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    targetUrl = json['targetUrl'];
    isOk = json['isOk'];
    error = json['error'];
    unAuthorizedRequest = json['unAuthorizedRequest'];
    bAbp = json['__abp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['targetUrl'] = this.targetUrl;
    data['isOk'] = this.isOk;
    data['error'] = this.error;
    data['unAuthorizedRequest'] = this.unAuthorizedRequest;
    data['__abp'] = this.bAbp;
    return data;
  }
}
