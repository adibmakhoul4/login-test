
import 'AreaModel.dart';
import 'CityModel.dart';
import 'ErrorModel.dart';
import 'messageModel.dart';

class LoginResponceModel {
  int resultCode;
  String resultText;
  String token;
  bool isOk;
  AreaModel areaModel;
  MessageModel messageModel;
  CityModel cityModel;
  ErrorModel errorModel;
  LoginResponceModel({ this.resultCode,  this.resultText,this.token,this.isOk,this.messageModel,this.errorModel});

  LoginResponceModel.fromJson(Map<String, dynamic> json) {
    resultCode = json['resultCode'];
    resultText = json['resultText'];
    token = json['token'];
    isOk = json['isOk'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resultCode'] = this.resultCode;
    data['resultText'] = this.resultText;
    data['token'] = this.token;
    data['isOk'] = this.isOk;

    return data;
  }
}