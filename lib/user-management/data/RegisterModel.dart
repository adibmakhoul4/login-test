class RegisterModel {
  String phone;
  String password;
  String first_name;
  String last_name;
  String email;

  var gender;

  RegisterModel(
      {this.phone,
      this.password,
      this.first_name,
      this.last_name,
      this.email,
      this.gender
      });

  RegisterModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    password = json['password'];
    first_name = json['first_name'];
    last_name = json['last_name'];
    email = json['email'];
    gender = json['gender'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['first_name'] = this.first_name;
    data['last_name'] = this.last_name;
    data['email'] = this.email;
    data['gender'] = this.gender;

    return data;
  }
}
