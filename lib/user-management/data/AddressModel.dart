
import 'AreaModel.dart';

class AddressModel {
  int id;
  String phone;
  String address;
  var lat;
  var lng;
  AreaModel areaModel;

  AddressModel({ this.id,  this.phone, this.address,this.lat,this.lng});

  AddressModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phone = json['phone'];
    address = json['address'];
       lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['phone'] = this.phone;
    data['address'] = this.address;
        data['lat'] = this.lat;
        data['lng'] = this.lng;

    return data;
  }
}