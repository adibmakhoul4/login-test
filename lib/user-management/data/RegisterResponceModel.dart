
import 'ErrorModel.dart';

class RegisterResponceModel {
  bool isOk;
  ErrorModel error;

  RegisterResponceModel(
      {this.isOk,
      this.error
    });

  RegisterResponceModel.fromJson(Map<String, dynamic> json) {
    isOk =
        json['isOk'];
      error =
        json['error'] != null ? new ErrorModel.fromJson(json['error']) : null;

  }
}

class Result {
  bool canLogin;

  Result({this.canLogin});

  Result.fromJson(Map<String, dynamic> json) {
    canLogin = json['canLogin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['canLogin'] = this.canLogin;
    return data;
  }
}
