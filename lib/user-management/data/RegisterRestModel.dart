class RegisterRestModel {
  String name;
  String emailAddress;
  String password;
  String phoneNumber;
  String commercialRegisterNumber;
  String commercialRegisterDocument;
  int cityId;
  String managerName;
  String managerPhoneNumber;
  String managerCountryCode;


  RegisterRestModel(
      {this.name,
      this.emailAddress,
      this.password,
      this.phoneNumber,
      this.commercialRegisterNumber,
      this.commercialRegisterDocument,
      this.cityId,
      this.managerName,
      this.managerPhoneNumber,this.managerCountryCode
      });

  RegisterRestModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    emailAddress = json['emailAddress'];
    password = json['password'];
    phoneNumber = json['phoneNumber'];

    commercialRegisterNumber = json['commercialRegisterNumber'];
    commercialRegisterDocument = json['commercialRegisterDocument'];
    cityId = json['cityId'];
    managerName = json['managerName'];
    managerPhoneNumber = json['managerPhoneNumber'];
    managerCountryCode = json['managerCountryCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['commercialRegisterNumber'] = this.commercialRegisterNumber;
    data['commercialRegisterDocument'] = this.commercialRegisterDocument;
    data['password'] = this.password;
    data['phoneNumber'] = this.phoneNumber;


      data['name'] = this.name;
    data['emailAddress'] = this.emailAddress;
    data['cityId'] = this.cityId;
    data['managerName'] = this.managerName;
       data['managerPhoneNumber'] = this.managerPhoneNumber;
    data['managerCountryCode'] = this.managerCountryCode;
    return data;
  }
}
