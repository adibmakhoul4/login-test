part of 'user_mangment_cubit.dart';

@immutable
abstract class UserMangmentState {}

class UserMangmentInitial extends UserMangmentState {}

// login state
class LoginInitial extends UserMangmentState {}

class LoginLoading extends UserMangmentState {}

class LoginSucess extends UserMangmentState {
  final LoginResponceModel loginResponceModel;
  LoginSucess({ this.loginResponceModel});
}

class LoginFailur extends UserMangmentState {
  final ErrorModel errorModel;
  LoginFailur({  this.errorModel});
}

class GmailLoading extends UserMangmentState {}

class GmailSucess extends UserMangmentState {
  GmailSucess();
}

class GmailFailur extends UserMangmentState {
  final ErrorModel errorModel;
  GmailFailur({  this.errorModel});
}
//Register states

class RegisterSucess extends UserMangmentState {
  final RegisterResponceModel registerResponceModel;
  RegisterSucess({ this.registerResponceModel});
}

class RegisterLoading extends UserMangmentState {}

class RegisterFailur extends UserMangmentState {
  final ErrorModel errorModel;
  RegisterFailur({  this.errorModel});
}
