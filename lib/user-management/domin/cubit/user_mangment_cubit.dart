
import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meta/meta.dart';
import 'package:test_app/core/API/Network.dart';
import 'package:test_app/core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:test_app/user-management/data/ErrorModel.dart';
import 'package:test_app/user-management/data/LoginModel.dart';
import 'package:test_app/user-management/data/LoginResponceModel.dart';
import 'package:test_app/user-management/data/RegisterModel.dart';
import 'package:test_app/user-management/data/RegisterResponceModel.dart';


part 'user_mangment_state.dart';

class UserMangmentCubit extends Cubit<UserMangmentState> with  ChangeNotifier{
  UserMangmentCubit() : super(UserMangmentInitial());
final googleSignIn = GoogleSignIn();

GoogleSignInAccount _user;
GoogleSignInAccount get user =>_user;

   void register(RegisterModel registerModel) async {
    RegisterResponceModel registerResponceModel;
    emit(RegisterLoading());
    try {
      registerResponceModel = await Network.register(registerModel);
      if (registerResponceModel.isOk ?? false) {
        emit(RegisterSucess(registerResponceModel: registerResponceModel));
      } else {
        emit(RegisterFailur(
            errorModel: registerResponceModel.error ??
                ErrorModel(message: "لم يتم التسجيل")));
      }
    } catch (e) {
      emit(RegisterFailur(errorModel: ErrorModel(message: e.toString())));
    }
  }
 Future<UserCredential> signWithGoogle() async{

  final GoogleSignInAccount googleUser =await GoogleSignIn(scopes: <String>["email"]).signIn();
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
  final OAuthCredential credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  //   final GoogleAuthCredential credential = GoogleAuthProvider.credential(
  //   accessToken: googleAuth.accessToken,
  //   idToken: googleAuth.idToken,
  // );
return await FirebaseAuth.instance.signInWithCredential(credential);
}

  void login({ LoginModel loginModel, BuildContext context}) async {
    LoginResponceModel loginResponceModel;
    emit(LoginLoading());
    try {
      loginResponceModel = await Network.login(loginModel);
      if (loginResponceModel.isOk?? false) {
        AppSharedPreferences.saveToken('exist');
        Network.initial();
        emit(LoginSucess(loginResponceModel: loginResponceModel));
      } else {
        emit(LoginFailur(
            errorModel: loginResponceModel.errorModel ??
                ErrorModel(message: loginResponceModel.resultText)));
      }
    } catch (e) {
      print(e.toString());
      emit(LoginFailur(errorModel: ErrorModel(message: e.toString())));
    }
  }

  }

