
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app/user-management/login.dart';
import 'package:test_app/user-management/welcome.dart';

import 'core/API/Network.dart';
import 'core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:firebase_core/firebase_core.dart';
Future<void> main() async {
   WidgetsFlutterBinding.ensureInitialized();
   await Firebase.initializeApp();
  await AppSharedPreferences.init();
  Network.initial();
  runApp(MyApp());
}
class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
    @override
  void initState() {
    super.initState();

  }

  // void showNotification() {
   
  //   flutterLocalNotificationsPlugin.show(
  //       0,
  //       "Testing ",
  //       "How you doin ?",
  //       NotificationDetails(
  //           android: AndroidNotificationDetails(channel.id, channel.name,
  //               importance: Importance.high,
  //               color: Colors.blue,
  //               playSound: true,
  //               icon: '@mipmap/ic_launcher')));
  // }

   @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'ANK',
            theme: ThemeData(fontFamily: 'Cairo',primarySwatch: Colors.red),
      darkTheme: ThemeData(
          brightness: Brightness.dark, primarySwatch: Colors.red),
              
          home: Sign(),
        );
      },
    );
  }

}
